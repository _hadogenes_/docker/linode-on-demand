#!/bin/bash

inotify_args=()

if [ "$VERBOSE" = "1" ]; then
    set -x
else
    inotify_args+=(--quiet --quiet)
fi

REQUEST_DIR=/request

LINODE_STATUS_CREATE=create
LINODE_STATUS_CONNECTING=connecting
LINODE_STATUS_READY=ready

getLinodeStatus() {
    linode_label="$1"
    cat "/tmp/linode.status/$linode_label"
}

isLinodeReady() {
    linode_label="$1"
    [ "$(getLinodeStatus "$linode_label")" = "$LINODE_STATUS_READY" ]
}

setLinodeStatus() {
    linode_label="$1"
    linode_status="$2"

    echo "$linode_status" > "/tmp/linode.status/$linode_label"
}

linodeCreate() {
    linode_label="$1"
    linode_type=$(sed -n '1,1p' "$REQUEST_DIR/$linode_label")

    setLinodeStatus "$linode_label" "$LINODE_STATUS_CREATE"

    linode-cli linodes create \
        --stackscript_id "$LINODE_STACKSCRIPT_ID" \
        --region "$LINODE_REGION" \
        --type "$linode_type" \
        --image "$LINODE_IMAGE" \
        --root_pass "$(openssl rand -base64 30)" \
        --authorized_users "$LINODE_AUTHORIZED_USERS" \
        --label "$linode_label" > /dev/null

    linode_id=$(linode-cli linodes list --format 'id,label' --text  --delimiter " " | awk '$2 == "'$linode_label'" { print $1 }')

    while sleep 3; do
        linode-cli linodes list --format 'id,status' --text --delimiter ";" | grep -q "$linode_id;running" && break
    done

    sleep 90

    sshToLinode "$linode_label"
}

sshToLinode() {
    linode_label="$1"
    setLinodeStatus "$linode_label" "$LINODE_STATUS_CONNECTING"

    ssh_args=(
        -f 
        -o "StrictHostKeyChecking no"
        -S "/tmp/.ssh.$linode_label.sock"
    )

    ssh_args+=( $(sed -n '2,2p' "$REQUEST_DIR/$linode_label") )
    ssh_cmd=$(sed -n '3,3p' "$REQUEST_DIR/$linode_label")

    linode_ipv4=$(linode-cli linodes list --format 'id,ipv4' --text  --delimiter " " | awk '$1 == "'$linode_id'" { print $2 }')

    for ((i=0; i<100; ++i)); do
        sleep 5
        ssh "${ssh_args[@]}" "$LINODE_REMOTE_USER@$linode_ipv4" "$ssh_cmd" && break
    done

    setLinodeStatus "$linode_label" "$LINODE_STATUS_READY"
}

doRequest() {
    linde_list_file=$(mktemp -u /tmp/linode.list.XXXXXX)
    linode-cli linodes list --format 'label,id' --text  --delimiter " " > "$linde_list_file"

    for linode_label in $LINODE_INSTANCES_LIST; do
        linode_id=$(awk '$1 == "'$linode_label'" { print $2 }' "$linde_list_file")
        if [ -f "$REQUEST_DIR/$linode_label" ]; then
            if [ ! "$linode_id" ]; then
                linodeCreate "$linode_label" &
            elif isLinodeReady "$linode_label" && [ ! -S "/tmp/.ssh.$linode_label.sock" ]; then
                sshToLinode "$linode_label" &
            elif [ ! "$(getLinodeReady)" ]; then    
                sshToLinode "$linode_label" &
            fi
        else
            if [ "$linode_id" ]; then
                linode-cli linodes delete "$linode_id" &
                setLinodeStatus "$linode_label" ""
            fi
        fi
    done

    rm -f "$linde_list_file"
}

inotify_args+=(
    --event create
    --event delete
    "$REQUEST_DIR"
)

if [ "$INOTIFY_TIMEOUT" ]; then
    inotify_args+=(--timeout "$INOTIFY_TIMEOUT")
fi

mkdir -p /tmp/linode.status
for linode_label in $LINODE_INSTANCES_LIST; do
    touch "/tmp/linode.status/$linode_label"
done

while true; do
    doRequest
    inotifywait "${inotify_args[@]}" &
    wait $!
done


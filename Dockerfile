FROM debian:stable-slim

ENV INOTIFY_TIMEOUT=600 \
    LINODE_REMOTE_USER=root

RUN apt update \
 && apt install -y \
        python3 \
        python3-requests \
        python3-terminaltables \
        python3-yaml \
        python3-pip \
        openssh-client \
        procps \
        inotify-tools \
 && apt clean \
 && pip install --break-system-packages linode-cli \
 && rm -rf /var/cache/apt/* \
 && rm -rf /var/lib/apt/lists/*

COPY linode-config /root/.config/linode-config
COPY run.sh /run.sh
RUN chmod 0755 /run.sh

CMD [ "/run.sh" ]
